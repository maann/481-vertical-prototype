﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace To_do_Prototype
{
    /// <summary>
    /// Interaction logic for WeeklyView.xaml
    /// </summary>
    public partial class WeeklyView : UserControl
    {
        
        private WeeklyView me;
        

        public WeeklyView()
        {
            InitializeComponent();
            //UserPoints = 0;
        }
        public void Itself(WeeklyView me)
        {
            this.me = me;
        }
        public void populateTasks()
        {
            //initialize day labels
            Label monday = new Label();
            monday.Content = "Mon, Mar 14 (Today)";    
            Label tuesday = new Label();
            tuesday.Content = "Tues, Mar 15";
            Label wednesday = new Label();
            wednesday.Content = "Wed, Mar 16";
            Label thursday = new Label();
            thursday.Content = "Thurs, Mar 17";
            Label friday = new Label();
            friday.Content = "Fri, Mar 18";
            Label saturday = new Label();
            saturday.Content = "Sat, Mar 19";
            Label sunday = new Label();
            sunday.Content = "Sun, Mar 20";

            //initialize tasks
            WeeklyTaskInsert task1 = new WeeklyTaskInsert();
            //task1.WeeklyViewReference(me);
            task1.TaskTitle = "Feed Max";

            WeeklyTaskInsert task2 = new WeeklyTaskInsert();
            task2.TaskTitle = "Assignment1";

            WeeklyTaskInsert task3 = new WeeklyTaskInsert();
            task3.TaskTitle = "Study For Midterm";

            WeeklyTaskInsert task4 = new WeeklyTaskInsert();
            task4.TaskTitle = "Buy groceries";

            //spaceholder
            Canvas spaceholder1 = new Canvas();
            spaceholder1.Height = 20;
            spaceholder1.Width = 135;

            Canvas spaceholder2 = new Canvas();
            spaceholder2.Height = 80;
            spaceholder2.Width = 135;

            Canvas spaceholder3 = new Canvas();
            spaceholder3.Height = 80;
            spaceholder3.Width = 135;

            Canvas spaceholder4 = new Canvas();
            spaceholder4.Height = 20;
            spaceholder4.Width = 135;

            //LEFT SIDE
            //monday
            LeftSide.Children.Add(monday);
            LeftSide.Children.Add(task1);
            LeftSide.Children.Add(task2);

            //wednesday
            LeftSide.Children.Add(wednesday);
            LeftSide.Children.Add(task3);
            LeftSide.Children.Add(task4);

            //friday
            LeftSide.Children.Add(friday);

            LeftSide.Children.Add(spaceholder4);
            //sunday
            LeftSide.Children.Add(sunday);

            //RIGHT SIDE
            RightSide.Children.Add(spaceholder1);
            RightSide.Children.Add(tuesday);
            RightSide.Children.Add(spaceholder2);            
            RightSide.Children.Add(thursday);
            RightSide.Children.Add(spaceholder3);
            RightSide.Children.Add(saturday);
        }   

        //public void AddPoints()
        //{
        //    userPoints += 1000;
        //    PointsText.Content = String.Concat("Points: ", userPoints.ToString());
        //}
    }
}
